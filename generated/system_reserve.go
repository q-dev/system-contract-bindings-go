// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package generated

import (
	"errors"
	"math/big"
	"strings"

	ethereum "gitlab.com/q-dev/q-client"
	"gitlab.com/q-dev/q-client/accounts/abi"
	"gitlab.com/q-dev/q-client/accounts/abi/bind"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/core/types"
	"gitlab.com/q-dev/q-client/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// SystemReserveMetaData contains all meta data concerning the SystemReserve contract.
var SystemReserveMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"inputs\":[],\"name\":\"addQEURStableCoin\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getAvailableAmount\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getCoolDownPhase\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getEligibleContractKeys\",\"outputs\":[{\"internalType\":\"string[]\",\"name\":\"\",\"type\":\"string[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getSystemPaused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"registry_\",\"type\":\"address\"},{\"internalType\":\"string[]\",\"name\":\"keys_\",\"type\":\"string[]\"}],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"state_\",\"type\":\"bool\"}],\"name\":\"setPauseState\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"amount_\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"stateMutability\":\"payable\",\"type\":\"receive\"}]",
	Bin: "0x608060405234801561001057600080fd5b50611297806100206000396000f3fe60806040526004361061006f5760003560e01c80632d250a5b1461007b5780632e1a7d4d146100a65780637bb476f5146100d657806394e6f56f146100f5578063a75c13bb14610117578063a866d47f1461012c578063cdb88ad114610144578063dfc769d41461016457600080fd5b3661007657005b600080fd5b34801561008757600080fd5b50610090610179565b60405161009d9190610e12565b60405180910390f35b3480156100b257600080fd5b506100c66100c1366004610e74565b610252565b604051901515815260200161009d565b3480156100e257600080fd5b506004545b60405190815260200161009d565b34801561010157600080fd5b50610115610110366004610ee9565b610599565b005b34801561012357600080fd5b506003546100e7565b34801561013857600080fd5b5060025460ff166100c6565b34801561015057600080fd5b5061011561015f366004611024565b6106a4565b34801561017057600080fd5b50610115610992565b60606001805480602002602001604051908101604052809291908181526020016000905b828210156102495783829060005260206000200180546101bc90611048565b80601f01602080910402602001604051908101604052809291908181526020018280546101e890611048565b80156102355780601f1061020a57610100808354040283529160200191610235565b820191906000526020600020905b81548152906001019060200180831161021857829003601f168201915b50505050508152602001906001019061019d565b50505050905090565b6001546000908190815b81811015610331576000546001805433926201000090046001600160a01b031691633fb90271918590811061029357610293611083565b906000526020600020016040518263ffffffff1660e01b81526004016102b99190611099565b60206040518083038186803b1580156102d157600080fd5b505afa1580156102e5573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906103099190611141565b6001600160a01b031614156103215760019250610331565b61032a81611174565b905061025c565b50816103b85760405162461bcd60e51b815260206004820152604560248201527f5b5145432d3031383030305d2d5065726d697373696f6e2064656e696564202d60448201527f206f6e6c7920656c696769626c6520636f6e747261637473206861766520616360648201526431b2b9b99760d91b608482015260a4015b60405180910390fd5b60025460ff16156104285760405162461bcd60e51b815260206004820152603460248201527f5b5145432d3031383030325d2d5468652073797374656d20726573657276652060448201527331b7b73a3930b1ba1034b9903830bab9b2b2171760611b60648201526084016103af565b610430610a7a565b8347108061043c575083155b1561044a5760009250610592565b8360045410156104be5760405162461bcd60e51b815260206004820152603960248201527f5b5145432d3031383030335d2d496e73756666696369656e742066756e64732060448201527830bb30b4b630b13632903337b9103bb4ba34323930bbb0b61760391b60648201526084016103af565b83600460008282546104d0919061118f565b9091555050604051600090339086908381818185875af1925050503d8060008114610517576040519150601f19603f3d011682016040523d82523d6000602084013e61051c565b606091505b505090508061058c5760405162461bcd60e51b815260206004820152603660248201527f5b5145432d3031383030345d2d5472616e73666572206f66207468652077697460448201527534323930bbb0b61030b6b7bab73a103330b4b632b21760511b60648201526084016103af565b60019350505b5050919050565b600054610100900460ff16806105b2575060005460ff16155b6106155760405162461bcd60e51b815260206004820152602e60248201527f496e697469616c697a61626c653a20636f6e747261637420697320616c72656160448201526d191e481a5b9a5d1a585b1a5e995960921b60648201526084016103af565b600054610100900460ff16158015610637576000805461ffff19166101011790555b6000805462010000600160b01b031916620100006001600160a01b03861602179055815161066c906001906020850190610c7c565b50610675610aa7565b61067f90426111a6565b60035561068a610b5d565b600455801561069f576000805461ff00191690555b505050565b600060029054906101000a90046001600160a01b03166001600160a01b0316633fb9027160405180606001604052806023815260200161123f602391396040518263ffffffff1660e01b81526004016106fd91906111be565b60206040518083038186803b15801561071557600080fd5b505afa158015610729573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061074d9190611141565b6001600160a01b031663a230c524336040518263ffffffff1660e01b815260040161077891906111d1565b60206040518083038186803b15801561079057600080fd5b505afa1580156107a4573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906107c891906111e5565b806108f557506000546040805180820182526014815273676f7665726e616e63652e726f6f744e6f64657360601b60208201529051633fb9027160e01b8152620100009092046001600160a01b031691633fb902719161082a916004016111be565b60206040518083038186803b15801561084257600080fd5b505afa158015610856573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061087a9190611141565b6001600160a01b031663a230c524336040518263ffffffff1660e01b81526004016108a591906111d1565b60206040518083038186803b1580156108bd57600080fd5b505afa1580156108d1573d6000803e3d6000fd5b505050506040513d601f19601f820116820180604052508101906108f591906111e5565b61097f5760405162461bcd60e51b815260206004820152604f60248201527f5b5145432d3031383030315d2d5065726d697373696f6e2064656e696564202d60448201527f206f6e6c7920726f6f74206e6f64657320616e6420455051464920657870657260648201526e3a39903430bb329030b1b1b2b9b99760891b608482015260a4016103af565b6002805460ff1916911515919091179055565b6001805414610a055760405162461bcd60e51b815260206004820152603960248201527f5b5145432d3031383030355d2d546865205145555220737461626c6520636f6960448201527837103430b99030b63932b0b23c903132b2b71030b23232b21760391b60648201526084016103af565b60018054808201825560009190915260408051808201909152601b8082527a3232b3349728a2aaa91739bcb9ba32b6a232b13a20bab1ba34b7b760291b6020909201918252610a77927fb10e2d527612073b26eecdfd717e6a320cf44b4afac2b0732d9fcbe2b7fa0cf6019190610cd9565b50565b426003541015610aa557610a8c610aa7565b610a9690426111a6565b600355610aa1610b5d565b6004555b565b6000610ab1610bd2565b60405162498bff60e81b815260206004820152601f60248201527f676f7665726e65642e45505146492e72657365727665436f6f6c446f776e500060448201526001600160a01b03919091169063498bff00906064015b60206040518083038186803b158015610b2057600080fd5b505afa158015610b34573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610b589190611202565b905090565b6000610b67610bd2565b60405162498bff60e81b815260206004820152602760248201527f676f7665726e65642e45505146492e72657365727665436f6f6c446f776e54686044820152661c995cda1bdb1960ca1b60648201526001600160a01b03919091169063498bff0090608401610b08565b60008060029054906101000a90046001600160a01b03166001600160a01b0316633fb9027160405180606001604052806023815260200161121c602391396040518263ffffffff1660e01b8152600401610c2c91906111be565b60206040518083038186803b158015610c4457600080fd5b505afa158015610c58573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610b589190611141565b828054828255906000526020600020908101928215610cc9579160200282015b82811115610cc95782518051610cb9918491602090910190610cd9565b5091602001919060010190610c9c565b50610cd5929150610d59565b5090565b828054610ce590611048565b90600052602060002090601f016020900481019282610d075760008555610d4d565b82601f10610d2057805160ff1916838001178555610d4d565b82800160010185558215610d4d579182015b82811115610d4d578251825591602001919060010190610d32565b50610cd5929150610d76565b80821115610cd5576000610d6d8282610d8b565b50600101610d59565b5b80821115610cd55760008155600101610d77565b508054610d9790611048565b6000825580601f10610da7575050565b601f016020900490600052602060002090810190610a779190610d76565b6000815180845260005b81811015610deb57602081850181015186830182015201610dcf565b81811115610dfd576000602083870101525b50601f01601f19169290920160200192915050565b6000602080830181845280855180835260408601915060408160051b870101925083870160005b82811015610e6757603f19888603018452610e55858351610dc5565b94509285019290850190600101610e39565b5092979650505050505050565b600060208284031215610e8657600080fd5b5035919050565b6001600160a01b0381168114610a7757600080fd5b634e487b7160e01b600052604160045260246000fd5b604051601f8201601f1916810167ffffffffffffffff81118282101715610ee157610ee1610ea2565b604052919050565b6000806040808486031215610efd57600080fd5b8335610f0881610e8d565b925060208481013567ffffffffffffffff80821115610f2657600080fd5b8187019150601f8881840112610f3b57600080fd5b823582811115610f4d57610f4d610ea2565b8060051b610f5c868201610eb8565b918252848101860191868101908c841115610f7657600080fd5b87870192505b8383101561100357823586811115610f945760008081fd5b8701603f81018e13610fa65760008081fd5b8881013587811115610fba57610fba610ea2565b610fcb818801601f19168b01610eb8565b8181528f8c838501011115610fe05760008081fd5b818c84018c83013760009181018b01919091528352509187019190870190610f7c565b8099505050505050505050509250929050565b8015158114610a7757600080fd5b60006020828403121561103657600080fd5b813561104181611016565b9392505050565b600181811c9082168061105c57607f821691505b6020821081141561107d57634e487b7160e01b600052602260045260246000fd5b50919050565b634e487b7160e01b600052603260045260246000fd5b600060208083526000845481600182811c9150808316806110bb57607f831692505b8583108114156110d957634e487b7160e01b85526022600452602485fd5b8786018381526020018180156110f6576001811461110757611132565b60ff19861682528782019650611132565b60008b81526020902060005b8681101561112c57815484820152908501908901611113565b83019750505b50949998505050505050505050565b60006020828403121561115357600080fd5b815161104181610e8d565b634e487b7160e01b600052601160045260246000fd5b60006000198214156111885761118861115e565b5060010190565b6000828210156111a1576111a161115e565b500390565b600082198211156111b9576111b961115e565b500190565b6020815260006110416020830184610dc5565b6001600160a01b0391909116815260200190565b6000602082840312156111f757600080fd5b815161104181611016565b60006020828403121561121457600080fd5b505191905056fe676f7665726e616e63652e657870657274732e45505146492e706172616d6574657273676f7665726e616e63652e657870657274732e45505146492e6d656d62657273686970a26469706673582212209927c119e036a9cc4cf7f5fa2a964307cac446baeb1d337f0068ce8a4bf7961564736f6c63430008090033",
}

// SystemReserveABI is the input ABI used to generate the binding from.
// Deprecated: Use SystemReserveMetaData.ABI instead.
var SystemReserveABI = SystemReserveMetaData.ABI

// SystemReserveBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use SystemReserveMetaData.Bin instead.
var SystemReserveBin = SystemReserveMetaData.Bin

// DeploySystemReserve deploys a new Ethereum contract, binding an instance of SystemReserve to it.
func DeploySystemReserve(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *SystemReserve, error) {
	parsed, err := SystemReserveMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(SystemReserveBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &SystemReserve{SystemReserveCaller: SystemReserveCaller{contract: contract}, SystemReserveTransactor: SystemReserveTransactor{contract: contract}, SystemReserveFilterer: SystemReserveFilterer{contract: contract}}, nil
}

// SystemReserve is an auto generated Go binding around an Ethereum contract.
type SystemReserve struct {
	SystemReserveCaller     // Read-only binding to the contract
	SystemReserveTransactor // Write-only binding to the contract
	SystemReserveFilterer   // Log filterer for contract events
}

// SystemReserveCaller is an auto generated read-only Go binding around an Ethereum contract.
type SystemReserveCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SystemReserveTransactor is an auto generated write-only Go binding around an Ethereum contract.
type SystemReserveTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SystemReserveFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type SystemReserveFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SystemReserveSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type SystemReserveSession struct {
	Contract     *SystemReserve    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// SystemReserveCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type SystemReserveCallerSession struct {
	Contract *SystemReserveCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// SystemReserveTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type SystemReserveTransactorSession struct {
	Contract     *SystemReserveTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// SystemReserveRaw is an auto generated low-level Go binding around an Ethereum contract.
type SystemReserveRaw struct {
	Contract *SystemReserve // Generic contract binding to access the raw methods on
}

// SystemReserveCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type SystemReserveCallerRaw struct {
	Contract *SystemReserveCaller // Generic read-only contract binding to access the raw methods on
}

// SystemReserveTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type SystemReserveTransactorRaw struct {
	Contract *SystemReserveTransactor // Generic write-only contract binding to access the raw methods on
}

// NewSystemReserve creates a new instance of SystemReserve, bound to a specific deployed contract.
func NewSystemReserve(address common.Address, backend bind.ContractBackend) (*SystemReserve, error) {
	contract, err := bindSystemReserve(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &SystemReserve{SystemReserveCaller: SystemReserveCaller{contract: contract}, SystemReserveTransactor: SystemReserveTransactor{contract: contract}, SystemReserveFilterer: SystemReserveFilterer{contract: contract}}, nil
}

// NewSystemReserveCaller creates a new read-only instance of SystemReserve, bound to a specific deployed contract.
func NewSystemReserveCaller(address common.Address, caller bind.ContractCaller) (*SystemReserveCaller, error) {
	contract, err := bindSystemReserve(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SystemReserveCaller{contract: contract}, nil
}

// NewSystemReserveTransactor creates a new write-only instance of SystemReserve, bound to a specific deployed contract.
func NewSystemReserveTransactor(address common.Address, transactor bind.ContractTransactor) (*SystemReserveTransactor, error) {
	contract, err := bindSystemReserve(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SystemReserveTransactor{contract: contract}, nil
}

// NewSystemReserveFilterer creates a new log filterer instance of SystemReserve, bound to a specific deployed contract.
func NewSystemReserveFilterer(address common.Address, filterer bind.ContractFilterer) (*SystemReserveFilterer, error) {
	contract, err := bindSystemReserve(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SystemReserveFilterer{contract: contract}, nil
}

// bindSystemReserve binds a generic wrapper to an already deployed contract.
func bindSystemReserve(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SystemReserveABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SystemReserve *SystemReserveRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SystemReserve.Contract.SystemReserveCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SystemReserve *SystemReserveRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SystemReserve.Contract.SystemReserveTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SystemReserve *SystemReserveRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SystemReserve.Contract.SystemReserveTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_SystemReserve *SystemReserveCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _SystemReserve.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_SystemReserve *SystemReserveTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SystemReserve.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_SystemReserve *SystemReserveTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _SystemReserve.Contract.contract.Transact(opts, method, params...)
}

// GetAvailableAmount is a free data retrieval call binding the contract method 0x7bb476f5.
//
// Solidity: function getAvailableAmount() view returns(uint256)
func (_SystemReserve *SystemReserveCaller) GetAvailableAmount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _SystemReserve.contract.Call(opts, &out, "getAvailableAmount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetAvailableAmount is a free data retrieval call binding the contract method 0x7bb476f5.
//
// Solidity: function getAvailableAmount() view returns(uint256)
func (_SystemReserve *SystemReserveSession) GetAvailableAmount() (*big.Int, error) {
	return _SystemReserve.Contract.GetAvailableAmount(&_SystemReserve.CallOpts)
}

// GetAvailableAmount is a free data retrieval call binding the contract method 0x7bb476f5.
//
// Solidity: function getAvailableAmount() view returns(uint256)
func (_SystemReserve *SystemReserveCallerSession) GetAvailableAmount() (*big.Int, error) {
	return _SystemReserve.Contract.GetAvailableAmount(&_SystemReserve.CallOpts)
}

// GetCoolDownPhase is a free data retrieval call binding the contract method 0xa75c13bb.
//
// Solidity: function getCoolDownPhase() view returns(uint256)
func (_SystemReserve *SystemReserveCaller) GetCoolDownPhase(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _SystemReserve.contract.Call(opts, &out, "getCoolDownPhase")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetCoolDownPhase is a free data retrieval call binding the contract method 0xa75c13bb.
//
// Solidity: function getCoolDownPhase() view returns(uint256)
func (_SystemReserve *SystemReserveSession) GetCoolDownPhase() (*big.Int, error) {
	return _SystemReserve.Contract.GetCoolDownPhase(&_SystemReserve.CallOpts)
}

// GetCoolDownPhase is a free data retrieval call binding the contract method 0xa75c13bb.
//
// Solidity: function getCoolDownPhase() view returns(uint256)
func (_SystemReserve *SystemReserveCallerSession) GetCoolDownPhase() (*big.Int, error) {
	return _SystemReserve.Contract.GetCoolDownPhase(&_SystemReserve.CallOpts)
}

// GetEligibleContractKeys is a free data retrieval call binding the contract method 0x2d250a5b.
//
// Solidity: function getEligibleContractKeys() view returns(string[])
func (_SystemReserve *SystemReserveCaller) GetEligibleContractKeys(opts *bind.CallOpts) ([]string, error) {
	var out []interface{}
	err := _SystemReserve.contract.Call(opts, &out, "getEligibleContractKeys")

	if err != nil {
		return *new([]string), err
	}

	out0 := *abi.ConvertType(out[0], new([]string)).(*[]string)

	return out0, err

}

// GetEligibleContractKeys is a free data retrieval call binding the contract method 0x2d250a5b.
//
// Solidity: function getEligibleContractKeys() view returns(string[])
func (_SystemReserve *SystemReserveSession) GetEligibleContractKeys() ([]string, error) {
	return _SystemReserve.Contract.GetEligibleContractKeys(&_SystemReserve.CallOpts)
}

// GetEligibleContractKeys is a free data retrieval call binding the contract method 0x2d250a5b.
//
// Solidity: function getEligibleContractKeys() view returns(string[])
func (_SystemReserve *SystemReserveCallerSession) GetEligibleContractKeys() ([]string, error) {
	return _SystemReserve.Contract.GetEligibleContractKeys(&_SystemReserve.CallOpts)
}

// GetSystemPaused is a free data retrieval call binding the contract method 0xa866d47f.
//
// Solidity: function getSystemPaused() view returns(bool)
func (_SystemReserve *SystemReserveCaller) GetSystemPaused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _SystemReserve.contract.Call(opts, &out, "getSystemPaused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// GetSystemPaused is a free data retrieval call binding the contract method 0xa866d47f.
//
// Solidity: function getSystemPaused() view returns(bool)
func (_SystemReserve *SystemReserveSession) GetSystemPaused() (bool, error) {
	return _SystemReserve.Contract.GetSystemPaused(&_SystemReserve.CallOpts)
}

// GetSystemPaused is a free data retrieval call binding the contract method 0xa866d47f.
//
// Solidity: function getSystemPaused() view returns(bool)
func (_SystemReserve *SystemReserveCallerSession) GetSystemPaused() (bool, error) {
	return _SystemReserve.Contract.GetSystemPaused(&_SystemReserve.CallOpts)
}

// AddQEURStableCoin is a paid mutator transaction binding the contract method 0xdfc769d4.
//
// Solidity: function addQEURStableCoin() returns()
func (_SystemReserve *SystemReserveTransactor) AddQEURStableCoin(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SystemReserve.contract.Transact(opts, "addQEURStableCoin")
}

// AddQEURStableCoin is a paid mutator transaction binding the contract method 0xdfc769d4.
//
// Solidity: function addQEURStableCoin() returns()
func (_SystemReserve *SystemReserveSession) AddQEURStableCoin() (*types.Transaction, error) {
	return _SystemReserve.Contract.AddQEURStableCoin(&_SystemReserve.TransactOpts)
}

// AddQEURStableCoin is a paid mutator transaction binding the contract method 0xdfc769d4.
//
// Solidity: function addQEURStableCoin() returns()
func (_SystemReserve *SystemReserveTransactorSession) AddQEURStableCoin() (*types.Transaction, error) {
	return _SystemReserve.Contract.AddQEURStableCoin(&_SystemReserve.TransactOpts)
}

// Initialize is a paid mutator transaction binding the contract method 0x94e6f56f.
//
// Solidity: function initialize(address registry_, string[] keys_) returns()
func (_SystemReserve *SystemReserveTransactor) Initialize(opts *bind.TransactOpts, registry_ common.Address, keys_ []string) (*types.Transaction, error) {
	return _SystemReserve.contract.Transact(opts, "initialize", registry_, keys_)
}

// Initialize is a paid mutator transaction binding the contract method 0x94e6f56f.
//
// Solidity: function initialize(address registry_, string[] keys_) returns()
func (_SystemReserve *SystemReserveSession) Initialize(registry_ common.Address, keys_ []string) (*types.Transaction, error) {
	return _SystemReserve.Contract.Initialize(&_SystemReserve.TransactOpts, registry_, keys_)
}

// Initialize is a paid mutator transaction binding the contract method 0x94e6f56f.
//
// Solidity: function initialize(address registry_, string[] keys_) returns()
func (_SystemReserve *SystemReserveTransactorSession) Initialize(registry_ common.Address, keys_ []string) (*types.Transaction, error) {
	return _SystemReserve.Contract.Initialize(&_SystemReserve.TransactOpts, registry_, keys_)
}

// SetPauseState is a paid mutator transaction binding the contract method 0xcdb88ad1.
//
// Solidity: function setPauseState(bool state_) returns()
func (_SystemReserve *SystemReserveTransactor) SetPauseState(opts *bind.TransactOpts, state_ bool) (*types.Transaction, error) {
	return _SystemReserve.contract.Transact(opts, "setPauseState", state_)
}

// SetPauseState is a paid mutator transaction binding the contract method 0xcdb88ad1.
//
// Solidity: function setPauseState(bool state_) returns()
func (_SystemReserve *SystemReserveSession) SetPauseState(state_ bool) (*types.Transaction, error) {
	return _SystemReserve.Contract.SetPauseState(&_SystemReserve.TransactOpts, state_)
}

// SetPauseState is a paid mutator transaction binding the contract method 0xcdb88ad1.
//
// Solidity: function setPauseState(bool state_) returns()
func (_SystemReserve *SystemReserveTransactorSession) SetPauseState(state_ bool) (*types.Transaction, error) {
	return _SystemReserve.Contract.SetPauseState(&_SystemReserve.TransactOpts, state_)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 amount_) returns(bool)
func (_SystemReserve *SystemReserveTransactor) Withdraw(opts *bind.TransactOpts, amount_ *big.Int) (*types.Transaction, error) {
	return _SystemReserve.contract.Transact(opts, "withdraw", amount_)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 amount_) returns(bool)
func (_SystemReserve *SystemReserveSession) Withdraw(amount_ *big.Int) (*types.Transaction, error) {
	return _SystemReserve.Contract.Withdraw(&_SystemReserve.TransactOpts, amount_)
}

// Withdraw is a paid mutator transaction binding the contract method 0x2e1a7d4d.
//
// Solidity: function withdraw(uint256 amount_) returns(bool)
func (_SystemReserve *SystemReserveTransactorSession) Withdraw(amount_ *big.Int) (*types.Transaction, error) {
	return _SystemReserve.Contract.Withdraw(&_SystemReserve.TransactOpts, amount_)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_SystemReserve *SystemReserveTransactor) Receive(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _SystemReserve.contract.RawTransact(opts, nil) // calldata is disallowed for receive function
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_SystemReserve *SystemReserveSession) Receive() (*types.Transaction, error) {
	return _SystemReserve.Contract.Receive(&_SystemReserve.TransactOpts)
}

// Receive is a paid mutator transaction binding the contract receive function.
//
// Solidity: receive() payable returns()
func (_SystemReserve *SystemReserveTransactorSession) Receive() (*types.Transaction, error) {
	return _SystemReserve.Contract.Receive(&_SystemReserve.TransactOpts)
}
